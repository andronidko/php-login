<?php
ob_start();
// Set sessions
if(!isset($_SESSION)) {
    session_start();
}

$server = "localhost";
$user = "root";
$pass = "root";
$database = "ak_login_page";

$connect = new mysqli($server, $user, $pass, $database);
$connect->set_charset("utf8");
if ($connect->connect_error) {
    die('Connection failed: ' . $connect->connect_error);
}