<?php
include 'db.php';
if (isset($_SESSION['user_id'])) {
    header("Location: index.php",true, 302);
}

if(isset($_POST["sign_in"])) {

    $errors = [];

    $login          = !empty($_POST["login"])?$_POST["login"]:'';
    $password       = !empty($_POST["password"])?$_POST["password"]:'';

    if(empty($login)) {
        $errors['login'] = 'Login can not be blank';
    }
    elseif (!preg_match("/^[0-9a-zA-Z]*$/", $login)) {
        $errors['login'] = 'Only letters and digits allowed';
    }

    if(empty($password)) {
        $errors['password'] = 'Password can not be blank';
    }
    elseif (!preg_match("/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{6,20}$/", $password)) {
        $errors['password'] = 'Password should be between 6 to 20 charcters long, contains atleast one special chacter, lowercase, uppercase and a digit';
    }

    if (empty($errors)) {
        // check if login already exist
        $stmt = $connect->prepare("SELECT * FROM users WHERE login =?");
        $stmt->bind_param("s", $login);
        $stmt->execute();

        $result = $stmt->get_result();   // You get a result object now
        if($result->num_rows <= 0) {
            $errors[] = 'User account does not exist';
        }
        else {
            while ($row = $result->fetch_assoc()) {
                $id         = $row['id'];
                $firstname  = $row['fname'];
                $lastname   = $row['lname'];
                $email      = $row['email'];
                $username      = $row['login'];
                $hash       = $row['password'];
            }

            $verified = password_verify($password, $hash);

            if ($verified && $username == $login) {
                $_SESSION['user_id'] = $id;
                $_SESSION['firstname'] = $firstname;
                $_SESSION['lastname'] = $lastname;
                $_SESSION['email'] = $email;
                $_SESSION['login'] = $username;
                header("Location: index.php", true, 302);
            }
            else {
                 $errors[] = 'Either email or password is incorrect';
            }
        }
    }
}

$page_title = 'Sign In';
include 'header.php';
?>
<div class="main">
    <div class="v-center">
        <div class="inner-block">
            <form action="" method="post">
                <h3>Login</h3>
                <?php
                if (!empty($errors)) {
                echo '<div class="alert alert-danger" role="alert">';
                    foreach($errors as $err) {
                    echo '<p>' . $err . '</p>';
                    }
                    echo '</div>';
                } ?>
                <div class="form-group">
                    <label>Login</label>
                    <input type="text" class="form-control" name="login"/>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" />
                </div>
                <button type="submit" name="sign_in" class="btn btn-outline-primary btn-lg btn-block">Login</button>
                <p>Don't have an account? <a href="register.php">Register Here</a>.</p>
            </form>
        </div>
    </div>
</div>
