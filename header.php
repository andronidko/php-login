<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>PHP Test<?= isset($page_title) ? (' - ' . $page_title) : ''?></title>
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-weight: 400;
            background-color: #EEEFF4;
        }
        body, html, .main, .v-center {
            width: 100%;
            height: 100%;
        }
        .navbar {
            background: #1833FF !important;
            width: 100%;
        }
        .btn-outline-primary {
            border-color: #1833FF;
            color: #1833FF;
        }
        .btn-outline-primary:hover {
            background-color: #1833FF;
            color: #ffffff;
        }
        .v-center {
            display: flex;
            text-align: left;
            justify-content: center;
            flex-direction: column;
        }
        .inner-block {
            width: 450px;
            margin: auto;
            background: #ffffff;
            box-shadow: 0px 14px 80px rgba(34, 35, 58, 0.2);
            padding: 40px 55px 45px 55px;
            transition: all .3s;
            border-radius: 20px;
        }
        .v-center .form-control:focus {
            border-color: #2554FF;
            box-shadow: none;
        }
        .v-center h3 {
            text-align: center;
            margin: 0;
            line-height: 1;
            padding-bottom: 20px;
        }
        label {
            font-weight: 500;
        }
        .form-control.error {
            border: 1px solid red;
        }
    </style>


</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">PHP User Auth System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02"
                aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <?php if (empty($_SESSION['user_id'])): ?>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Log In</a>
                </li>
                <?php else : ?>
                <li class="nav-item">
                    <a class="nav-link" href="profile.php">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Log Out</a>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
