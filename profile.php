<?php

include 'db.php';

if (empty($_SESSION['user_id'])) {
    //header("HTTP/1.1 404 Not Found");
    header("Location: index.php", true, 302);
}

$firstname = $_SESSION['firstname'];
$lastname =  $_SESSION['lastname'];
$email = $_SESSION['email']; 
$login = $_SESSION['login'];
$password = $_SESSION['password'];

if(isset($_POST["submit"])) {
    $firstname      = !empty($_POST["fname"])?$_POST["fname"]:'';
    $lastname       = !empty($_POST["lname"])?$_POST["lname"]:'';
    $email          = !empty($_POST["email"])?$_POST["email"]:'';
    $login          = !empty($_POST["login"])?$_POST["login"]:'';
    $password       = !empty($_POST["password"])?$_POST["password"]:'';

    $errors = [];

    if(empty($firstname)) {
        $errors['fname'] = 'First name can not be blank';
    }
    elseif (!preg_match("/^[a-zA-Z ]*$/", $firstname)) {
        $errors['fname'] = 'Only letters and white space allowed';
    }
    if(empty($lastname)) {
        $errors['lname'] = 'Last name can not be blank';
    }
    elseif (!preg_match("/^[a-zA-Z ]*$/", $lastname)) {
        $errors['lname'] = 'Only letters and white space allowed';
    }

    if(empty($email)) {
        $errors['email'] = 'Email can not be blank';
    }
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = 'Email format is invalid';
    }

    if(empty($login)) {
        $errors['login'] = 'Login can not be blank';
    }
    elseif (!preg_match("/^[0-9a-zA-Z]*$/", $login)) {
        $errors['login'] = 'Only letters and digits allowed';
    }
    if(empty($password)) {
        $errors['password'] = 'Password can not be blank';
    }
    elseif ( $_SESSION['password'] !== $password && !preg_match("/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{6,20}$/", $password)) {
        $errors['password'] = 'Password should be between 6 to 20 charcters long, contains atleast one special chacter, lowercase, uppercase and a digit';
    }

    if (empty($errors)) {

        if ($_SESSION['password'] !== $password) {
            $password = password_hash($password);
        }

        $sql = "UPDATE users SET login=?, email=?, fname=?, lname=?, password=? WHERE id=?";

        if ($stmt = $connect->prepare($sql)) {
            if (!$stmt->bind_param('sssssi', $login, $email, $firstname, $lastname, $password)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
                die();
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                die();
            }
            else {       
                $_SESSION['firstname'] = $firstname;
                $_SESSION['lastname'] = $lastname;
                $_SESSION['email'] = $email; 
                $_SESSION['login'] = $login;
                $_SESSION['password'] = $password;
                header("Location: index.php",true,200);
            }
        }
        else {
            echo "Prepare failed: (" . $connect->errno . ") " . $connect->error;
            die();
        }
    }
}

$page_title = 'Profile';
include 'header.php';

?>
<div class="main">
    <div class="v-center">
        <div class="inner-block">
            <form action="" method="post">
                <h3>Profile</h3>
                <?php
                if (!empty($errors)) {
                    echo '<div class="alert alert-danger" role="alert">';
                    foreach($errors as $key => $err) {
                        echo '<p class="' . $key . '-error">' . $err . '</p>';
                    }
                    echo '</div>';
                }
                ?>
                <div class="form-group">
                    <label>First name</label>
                    <input type="text" class="form-control<?= empty($errors['fname'])?'':' error'; ?>" name="fname" id="firstName" value = "<?=  htmlentities($firstname, ENT_QUOTES, "UTF-8"); ?>"/>
                </div>
                <div class="form-group">
                    <label>Last name</label>
                    <input type="text" class="form-control<?= empty($errors['lname'])?'':' error'; ?>" name="lname" id="lastName" value = "<?=  htmlentities($lastname, ENT_QUOTES, "UTF-8"); ?>"/>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control<?= empty($errors['email'])?'':' error'; ?>" name="email" id="email" value = "<?=  htmlentities($email, ENT_QUOTES, "UTF-8"); ?>"/>
                </div>
                <div class="form-group">
                    <label>Login</label>
                    <input type="text" class="form-control<?= empty($errors['login'])?'':' error'; ?>" name="login" id="login" value = "<?=  htmlentities($login, ENT_QUOTES, "UTF-8"); ?>"/>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control<?= empty($errors['password'])?'':' error'; ?>" name="password" id="password" value = "<?=  htmlentities($password, ENT_QUOTES, "UTF-8"); ?>"/>
                </div>
                <button type="submit" name="submit" id="submit" class="btn btn-outline-primary btn-lg btn-block">Sign up</button>
            </form>
        </div>
    </div>
</div>
<?php include 'footer.php';