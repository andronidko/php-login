--
-- Table structure for table `users`
--

CREATE TABLE `users` (
     `id` int(11) NOT NULL  AUTO_INCREMENT,
     `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
     `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
     `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
     `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
     `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_login` (`login`),
  ADD KEY `user_email` (`email`);
COMMIT;


