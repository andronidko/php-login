    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {
           $('form input').on('change', function(e) {
               if ($(this).hasClass('error') ) {
                   let $name = $(this).attr('name');
                   let $parent = $('div.alert p.'+ $(this).attr('name') + '-error').closest('.alert');
                   $(this).removeClass('error');
                   $('div.alert p.'+ $(this).attr('name') + '-error').remove();
                   if ( $parent.is(':empty') ) {
                       $parent.remove();
                   }
               }
           })
        });
    </script>
</body>
</html>