<?php
error_reporting(E_ALL);
ini_set('display_errors','On');
include 'db.php';
include 'header.php';
?>
<div class="main">
    	<div class="v-center">
        	<div class="inner-block">
			<?php 	
			if (!empty($_SESSION['user_id'])) {
				echo '<h1>Welcome, ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] . '</h1>';
				echo '<a href="logout.php">Logout</a>';
			}
			else {
				echo '<h1>Welcome, guest</h1>';
				echo '<a href="login.php">Log In</a>';
			} ?>
		</div>
	</div>
</div>
<?php
include 'footer.php';